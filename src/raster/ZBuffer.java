package raster;

import transforms.Col;

public class ZBuffer {
    private ImageBuffer imageBuffer;
    private DepthBuffer depthBuffer;

    public ZBuffer(ImageBuffer imageBuffer) {
        this.imageBuffer = imageBuffer;
        this.depthBuffer = new DepthBuffer(imageBuffer.getWidth(), imageBuffer.getHeight());
    }

    public void drawWithZTest(int x, int y, double z, Col color){
        // TODO: Implementace zbufferu jako takového
        // Načtu hodnotu z depthBUfferu na pozici x,y
        // Načetnou hodnotu porovnám s Z, které vstoupilo do metody (nové)
        // pokud, je nové < než staré
        // tak -> obravím a nastavím nové z v paměti hloubky

        imageBuffer.setElement(x, y, color);
    }
}

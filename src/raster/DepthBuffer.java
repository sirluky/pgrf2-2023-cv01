package raster;

import java.util.Optional;

public class DepthBuffer implements Raster<Double>{
    private double[][] buffer;
    private int width, height;

    private double clearValue = 1;

    public DepthBuffer(int width, int height) {
        buffer = new double[width][height];

        this.width = width;
        this.height = height;
    }

    @Override
    public void clear() {
        // TODO: všude nastavit clearValue
    }

    @Override
    public void setClearElement(Double value) {
        this.clearValue = value;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Double getElement(int x, int y) {
        // TODO: implementace jako taková
        // TODO: kontrola mimo pole
        return null;
    }

    @Override
    public void setElement(int x, int y, Double value) {
        // TODO: co kontrola zapsání mimo pole?
        buffer[x][y] = value;
    }
}
